//
//  WeatherAppFiftyFiveTests.swift
//  WeatherAppFiftyFiveTests
//
//  Created by SaurabhGoyal on 22/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//

import XCTest
@testable import WeatherAppFiftyFive

class WeatherAppFiftyFiveTests: XCTestCase {

   // var viewControllerObject:ViewController = ViewController()
    let client = DarkSkyAPIManager()
    var apiModel:WeatherApiModel!
    
    override func setUp() {
        
    // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testAPIdataSucessfully (){
        
        let locationCoordinate = Coordinate(latitude: 37.8267, longitude: -122.4233)
        client.getCurrentWeather(at: locationCoordinate) { currentWeather, error in
            
            if error != nil {
                 XCTAssertTrue(currentWeather != nil)
            }
        
    }
 }
    
    func testParsing (){
        
       // if dictionary is empty than api model is not nil .
        let currentWeather = WeatherApiModel(dictionary: NSDictionary())
        XCTAssertTrue(currentWeather != nil)
    
            
        }
    


}
