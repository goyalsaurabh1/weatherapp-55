//
//  WeatherDeatilViewModel.swift
//  WeatherAppFiftyFive
//
//  Created by SaurabhGoyal on 23/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//

import Foundation
import UIKit

struct WeatherDeatilViewModel {
    var headerTitle: String
    var headerSubTitle: String
    var currentTemprautre: String
    var hourlyTempArray: [HourlyViewModel]
    var dailyTempArray: [DailyViewModel]
    var otherDetailsArray: [OtherDetailsViewModel]
    var currentWeatherSummary: String
    
    init(from model: WeatherApiModel) {
        
        headerTitle = "Test Title"
        
        if let currentlyModel:Currently =  model.currently{
            headerSubTitle = currentlyModel.summary ?? ""
            currentTemprautre = "\(currentlyModel.temperature?.toCelcius().int() ?? 0)º"
            otherDetailsArray = [OtherDetailsViewModel]()
            let humidityValue = currentlyModel.humidity?.percent().int()
            let precipValue = currentlyModel.precipProbability?.percent().int()
            let humidity = OtherDetailsViewModel(title: "Humidity", value: "\(humidityValue ?? 0)%")
            otherDetailsArray.append(humidity)
            let rain = OtherDetailsViewModel(title: "Rain", value: "\(precipValue ?? 0)%")
            otherDetailsArray.append(rain)
            let windSpeed = OtherDetailsViewModel(title: "Wind", value: "\(currentlyModel.windSpeed?.int() ?? 0) km/hr")
            otherDetailsArray.append(windSpeed)
            let pressure = OtherDetailsViewModel(title: "Pressure", value: "\(currentlyModel.pressure?.int() ?? 0) hPa")
            otherDetailsArray.append(pressure)
            let visibility = OtherDetailsViewModel(title: "Visibility", value: "\(currentlyModel.visibility?.int() ?? 0) km")
            otherDetailsArray.append(visibility)
            let uvIndex = OtherDetailsViewModel(title: "UV Index", value: "\(currentlyModel.uvIndex!)")
            otherDetailsArray.append(uvIndex)
            
        }
        else{
            currentTemprautre = ""
            headerSubTitle = ""
            otherDetailsArray = [OtherDetailsViewModel]()
        }
        if let hourlyModel : Hourly = model.hourly {
            
            hourlyTempArray = [HourlyViewModel]()
            if (hourlyModel.data != nil){
                for wData in hourlyModel.data! {
                    let hViewModel :HourlyViewModel = HourlyViewModel(from: wData)
                    if(hourlyTempArray.count<24){
                        hourlyTempArray.append(hViewModel)
                    }
                    else{
                        break;
                    }
                    
                }
            }
            
        }
        else{
            hourlyTempArray = [HourlyViewModel]()
        }
        
        if let dailyModel : Daily = model.daily {
            
            dailyTempArray = [DailyViewModel]()
            currentWeatherSummary = dailyModel.summary ?? ""
            if (dailyModel.data != nil){
                for dailyData in dailyModel.data! {
                    let dViewModel :DailyViewModel = DailyViewModel(from: dailyData)
                    dailyTempArray.append(dViewModel)
                    
                }
            }
            
        }
        else{
            currentWeatherSummary = ""
            dailyTempArray = [DailyViewModel]()
        }
        
    }
}

struct HourlyViewModel{
    var hourTime: String
    var hourIcon:UIImage
    var hourTemp: String
    
    init(from model: WeatherData) {
        
        let hourDateTime  = model.time?.toDate().toTimeString()
        hourTime = hourDateTime ?? ""
        let weatherIcon = WeatherIcon(iconString: model.icon ?? "")
        hourIcon = weatherIcon.image
        hourTemp = "\(model.temperature?.toCelcius().int() ?? 0)º"
    }
    
}

struct DailyViewModel{
    var weekDay: String
    var weekIcon:UIImage
    var weekMinTem: String
    var weekMaxTem: String
    
    init(from model: WeatherData) {
        
        let weekDayStr  = model.time?.toDate().toDayString()
        weekDay = weekDayStr ?? ""
        let weatherIcon = WeatherIcon(iconString: model.icon ?? "")
        weekIcon = weatherIcon.image
        weekMinTem = "\(model.temperatureMin?.toCelcius().int() ?? 0)º"
        weekMaxTem = "\(model.temperatureMax?.toCelcius().int() ?? 0)º"
    }
    
}

struct OtherDetailsViewModel{
    var title: String
    var value:String
}

