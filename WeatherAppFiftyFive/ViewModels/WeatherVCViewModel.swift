//
//  WeatherVCViewModel.swift
//  WeatherAppFiftyFive
//
//  Created by SaurabhGoyal on 23/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//

import Foundation
import UIKit

struct WeatherVCViewModel {
    let temperature: String
    let humidity: String
    let precipitationProbability: String
    let summary: String
    let icon: UIImage
    
    init(from model: WeatherApiModel) {
        
        if let currentWeather:Currently = model.currently{
            temperature = "\(currentWeather.temperature?.toCelcius().int() ?? 0)º"
            let humidityValue = currentWeather.humidity?.percent().int()
            humidity = "\(humidityValue ?? 0)%"
            
            let precipValue = currentWeather.precipProbability?.percent().int()
            precipitationProbability = "\(precipValue ?? 0)%"
            
            summary = currentWeather.summary ?? ""
            
            let weatherIcon = WeatherIcon(iconString: currentWeather.icon ?? "")
            icon = weatherIcon.image
        }
        else{
            temperature = ""
            humidity = ""
            precipitationProbability = ""
            summary = ""
            icon = #imageLiteral(resourceName: "clear-night")
        }
        
        
    }
}

extension Double {
    func percent() -> Double {
        return self * 100
    }
    
    func int() -> Int {
        return Int(self)
    }
    func toCelcius() -> Double {
        return (self - 32)/1.8
    }
    func toDate() -> Date {
        return Date(timeIntervalSince1970: self)
    }
}

extension Date{
    
    func toTimeString() -> String{
        let dateFromat = DateFormatter()
        dateFromat.dateFormat = "h a"
        let timeStr = dateFromat.string(from: self) // In your case its string1
        return timeStr
    }
    
    func toDayString() -> String{
        let dateFromat = DateFormatter()
        dateFromat.dateFormat = "EEEE"
        let timeStr = dateFromat.string(from: self) // In your case its string1
        return timeStr
    }
    
}


