//
//  DarkSkyAPIManager.swift
//  WeatherAppFiftyFive
//
//  Created by SaurabhGoyal on 22/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//

import Foundation

enum APIError: Error {
    case requestFailed
    case responseUnsuccessful
    case invalidData
    case invalidUrl
    case jsonConversionFailure
    case jsonParsingFailure
}

typealias CurrentWeatherCompletionHandler = (WeatherApiModel?, APIError?) -> Void

class DarkSkyAPIManager {
    lazy var baseURL: URL = {
        return URL(string: "https://api.darksky.net/forecast/\(APIKey.apiKey)/")!
    }()
    
    let netManager = NetworkManager()
    
    func getCurrentWeather(at coordinate: Coordinate, completionHandler completion: @escaping CurrentWeatherCompletionHandler) {
        guard let url = URL(string: coordinate.description, relativeTo: baseURL) else {
            completion(nil, .invalidUrl)
            return
        }
        
        let request = URLRequest(url: url)
        let task = netManager.jsonTaskWith(request: request) { json, error in
            DispatchQueue.main.async {
                guard let json = json else {
                    completion(nil, error)
                    return
                }
                
                
                guard let currentWeather = WeatherApiModel(dictionary: json as NSDictionary) else {
                    completion(nil, .jsonParsingFailure)
                    return
                }
                
                completion(currentWeather, nil)
            }
        }
        
        task.resume()
    }
}
