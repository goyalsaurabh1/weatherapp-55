//
//  DeatailViewController.swift
//  WeatherAppFiftyFive
//
//  Created by SaurabhGoyal on 23/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//

import UIKit
class DeatailViewController: UIViewController ,UITableViewDataSource {
    
    // MARK: View information UI elements
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblHeaderSubTitle: UILabel!
    @IBOutlet weak var weatherTableView: UITableView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    // MARK: Model variables
    var apiModel:WeatherApiModel!
    var viewModel:WeatherDeatilViewModel!
    
    // MARK: HeaderVariables
    var headerTitle:String!
    var headerSubTitle:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHeaderTitle.text = headerTitle
        lblHeaderSubTitle.text = headerSubTitle
        
        // Background Image shown according to time 
        let now = NSDate()
        let nowDateValue = now as Date
        let calendar = Calendar.current
        let time6pm = calendar.date(bySettingHour: 18, minute: 0, second: 0, of: nowDateValue)
        if (nowDateValue > time6pm ?? nowDateValue){
            backgroundImageView.image = UIImage(named: "nightClearBackground")
        }
        else{
            backgroundImageView.image = UIImage(named: "dayClearBackground")
        }
        
        viewModel = WeatherDeatilViewModel(from: apiModel)
        weatherTableView.dataSource = self;
    }
    
    // MARK: Table View Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0){
            return 1
        }
        else if (section == 1){
            return viewModel.dailyTempArray.count
        }
        else if (section == 2){
            return 1
        }
        else{
            return viewModel.otherDetailsArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section == 0){
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: "CurrentTemperatureTableViewCell", for: indexPath) as! CurrentTemperatureTableViewCell
            cell.lblTemprature.text = viewModel.currentTemprautre
            cell.hourArray = viewModel.hourlyTempArray
            cell.hourlyCollectionView.reloadData()
            
            
            return cell
        }
        else if (indexPath.section == 1){
            
            let dailyVM = viewModel.dailyTempArray[indexPath.row]
            let cell  = tableView.dequeueReusableCell(withIdentifier: "WeeklyTableViewCell", for: indexPath) as! WeeklyTableViewCell
            cell.icon.image = dailyVM.weekIcon
            cell.lblMinTemp.text = dailyVM.weekMinTem
            cell.lblMaxtemp.text = dailyVM.weekMaxTem
            cell.lblDay.text = dailyVM.weekDay
            return cell
            
        }
        else if (indexPath.section == 2){
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: "TodaySummaryTableViewCell", for: indexPath) as! TodaySummaryTableViewCell
            cell.lblTodaySummary.text = viewModel.currentWeatherSummary
            return cell
            
        }
        else{
            let otherDetailVM = viewModel.otherDetailsArray[indexPath.row]
            let cell  = tableView.dequeueReusableCell(withIdentifier: "OtherDetailsTableViewCell", for: indexPath) as! OtherDetailsTableViewCell
            cell.lblTitle.text = otherDetailVM.title
            cell.lblValue.text = otherDetailVM.value
            return cell
        }
    }
    
    
    // MARK: - Navigation
    
    @IBAction func backButtonMethod(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    /*
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
