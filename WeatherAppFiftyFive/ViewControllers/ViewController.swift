//
//  ViewController.swift
//  WeatherAppFiftyFive
//
//  Created by SaurabhGoyal on 22/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController  {
    
    // MARK: View information UI elements
    @IBOutlet weak var btnMoreDetails: UIButton!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var currentHumidityLabel: UILabel!
    @IBOutlet weak var currentPrecipitationLabel: UILabel!
    @IBOutlet weak var currentWeatherIcon: UIImageView!
    @IBOutlet weak var currentSummaryLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    // MARK: Location Variables
    let locationManager = CLLocationManager()
    var location: CLLocation?
    
    let geocoder = CLGeocoder()
    var placemark: CLPlacemark?
    
    // here I am declaring the iVars for city and country to access them later
    
    var city: String?
    var country: String?
    var countryShortName: String?
    
    // MARK: Model variables
    let client = DarkSkyAPIManager()    
    var apiModel:WeatherApiModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Background Image shown according to time
        let now = NSDate()
        let nowDateValue = now as Date
        let calendar = Calendar.current
        let time6pm = calendar.date(bySettingHour: 18, minute: 0, second: 0, of: nowDateValue)
        if (nowDateValue > time6pm ?? nowDateValue){
            backgroundImageView.image = UIImage(named: "nightClearBackground")
        }
        else{
            backgroundImageView.image = UIImage(named: "dayClearBackground")
        }
        
        // Location Realted code
        
        let authStatus = CLLocationManager.authorizationStatus()
        if authStatus == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
        
        if authStatus == .denied || authStatus == .restricted {
            //  alert or inform the user to to enable location services
        }
        
        startLocationManager()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Getting saved coordinates and calling API if available.
        
        let defaults = UserDefaults.standard
        if let coordinate = defaults.object(forKey: "coordiante") as? Data {
            let decoder = JSONDecoder()
            if let savedCoordinate = try? decoder.decode(Coordinate.self, from: coordinate) {
                
                let location = CLLocation(latitude: savedCoordinate.latitude, longitude: savedCoordinate.longitude)
                self.getCityData(location: location)
                displayWeather(for: savedCoordinate)
            }
        }
    }
    
    
    func handleError(message: String) {
        let alert = UIAlertController(title: "Error Loading Forecast", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayWeather(for coordinate: Coordinate) {
        
        client.getCurrentWeather(at: coordinate) { [unowned self] currentWeather, error in
            
            if error != nil {
                self.displayAlertWith(title: "Error Occurred!", message: "Sorry, an error occurred!")
                return
            }
            self.apiModel = currentWeather
            self.btnMoreDetails.isHidden = false
            let viewModel = WeatherVCViewModel(from: currentWeather!)
            self.displayWeather(using: viewModel)
        }
    }
    
    // MARK: UI display methods
    
    func displayWeather(using viewModel: WeatherVCViewModel) {
        currentTemperatureLabel.text = viewModel.temperature
        currentHumidityLabel.text = viewModel.humidity
        currentPrecipitationLabel.text = viewModel.precipitationProbability
        currentWeatherIcon.image = viewModel.icon
        currentSummaryLabel.text = viewModel.summary
    }
    
    
    func displayAlertWith(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "DetailViewSeague"){
            
            let detailVC = segue.destination as! DeatailViewController
            detailVC.headerTitle = self.city
            detailVC.headerSubTitle = currentSummaryLabel.text
            detailVC.apiModel = apiModel;
            
        }
        
    }
    
    
}

// MARK: Extension for loactioanmanager delegates and related method

extension ViewController: CLLocationManagerDelegate {
    
    func startLocationManager() {
        // always good habit to check if locationServicesEnabled
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func stopLocationManager() {
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // print the error to see what went wrong
        print("didFailwithError\(error)")
        // stop location manager if failed
        stopLocationManager()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let currentLocation = locations.last {
            
            let locationCoordinate = Coordinate(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
            
            let defaults = UserDefaults.standard
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(locationCoordinate) {
                
                defaults.set(encoded, forKey: "coordiante")
            }
            UserDefaults.standard.synchronize()
            displayWeather(for: locationCoordinate)
            
            // if you need to get latest data you can get locations.last to check it if the device has been moved
            let latestLocation = locations.last!
            
            // here check if no need to continue just return still in the same place
            if latestLocation.horizontalAccuracy < 0 {
                return
            }
            // if it location is nil or it has been moved
            if location == nil || location!.horizontalAccuracy > latestLocation.horizontalAccuracy {
                
                location = latestLocation
                // stop location manager
                stopLocationManager()
                
                // Here is the place you want to start reverseGeocoding
                self.getCityData(location: latestLocation)
            }
            
        }
        
    }
    
    func getCityData(location:CLLocation){
        
        geocoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
            // always good to check if no error
            // also we have to unwrap the placemark because it's optional
            // I have done all in a single if but you check them separately
            if error == nil, let placemark = placemarks, !placemark.isEmpty {
                self.placemark = placemark.last
            }
            // a new function where you start to parse placemarks to get the information you need
            self.parsePlacemarks()
            
        })
    }
    
    func parsePlacemarks() {
        // here we check if location manager is not nil using a _ wild card
        if let _ = location {
            // unwrap the placemark
            if let placemark = placemark {
                // wow now you can get the city name. remember that apple refers to city name as locality not city
                // again we have to unwrap the locality remember optionalllls also some times there is no text so we check that it should not be empty
                if let city = placemark.locality, !city.isEmpty {
                    // here you have the city name
                    // assign city name to our iVar
                    self.city = city
                }
                else{
                    self.city = ""
                }
                // the same story optionalllls also they are not empty
                if let country = placemark.country, !country.isEmpty {
                    
                    self.country = country
                }
                // get the country short name which is called isoCountryCode
                if let countryShortName = placemark.isoCountryCode, !countryShortName.isEmpty {
                    
                    self.countryShortName = countryShortName
                }
                else{
                    self.countryShortName = ""
                }
                
            }
            
            self.cityLabel.text = self.city! + ", " + self.countryShortName!
            
        } else {
            // add some more check's if for some reason location manager is nil
        }
        
    }
    
}

