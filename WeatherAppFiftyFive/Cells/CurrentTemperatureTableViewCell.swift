//
//  CurrentTemperatureTableViewCell.swift
//  WeatherAppFiftyFive
//
//  Created by SaurabhGoyal on 23/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//

import UIKit

class CurrentTemperatureTableViewCell: UITableViewCell , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var lblTemprature: UILabel!
    @IBOutlet weak var hourlyCollectionView: UICollectionView!
    var  hourArray:[HourlyViewModel]!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        hourlyCollectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hourArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourlyCollectionViewCell", for: indexPath) as! HourlyCollectionViewCell
        let currentHourVM = self.hourArray[indexPath.item]
         cell.lblHour.text = currentHourVM.hourTime
         cell.lblTemp.text = currentHourVM.hourTemp
         cell.icon.image = currentHourVM.hourIcon
        
        return cell
    }

}

class WeeklyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var lblMinTemp: UILabel!
    @IBOutlet weak var lblMaxtemp: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class TodaySummaryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTodaySummary: UILabel!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class OtherDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
