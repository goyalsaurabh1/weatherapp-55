//
//  HourlyCollectionViewCell.swift
//  WeatherAppFiftyFive
//
//  Created by SaurabhGoyal on 23/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//

import UIKit

class HourlyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblHour: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var lblTemp: UILabel!
}
