//
//  Daily.swift
//  WeatherAppFiftyFive
//
//  Created by SaurabhGoyal on 23/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//
import Foundation

public class Daily {
	public var summary : String?
	public var icon : String?
	public var data : Array<WeatherData>?


/**
    Constructs the object based on the given dictionary.
    

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Daily Instance.
*/
	required public init?(dictionary: NSDictionary) {

		summary = dictionary["summary"] as? String
		icon = dictionary["icon"] as? String
        if (dictionary["data"] != nil) { data = WeatherData.modelsFromDictionaryArray(array: dictionary["data"] as! NSArray) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.summary, forKey: "summary")
		dictionary.setValue(self.icon, forKey: "icon")

		return dictionary
	}

}
