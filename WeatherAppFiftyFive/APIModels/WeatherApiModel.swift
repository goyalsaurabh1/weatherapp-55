//
//  WeatherApiModel.swift
//  WeatherAppFiftyFive
//
//  Created by SaurabhGoyal on 23/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//


import Foundation
 

public class WeatherApiModel {

	public var currently : Currently?
	public var hourly : Hourly?
	public var daily : Daily?


/**
    Constructs the object based on the given dictionary.
    

    - parameter dictionary:  NSDictionary from JSON.

    - returns: WeatherApiModel Instance.
*/
	required public init?(dictionary: NSDictionary) {

		if (dictionary["currently"] != nil) { currently = Currently(dictionary: dictionary["currently"] as! NSDictionary) }
		
		if (dictionary["hourly"] != nil) { hourly = Hourly(dictionary: dictionary["hourly"] as! NSDictionary) }
		if (dictionary["daily"] != nil) { daily = Daily(dictionary: dictionary["daily"] as! NSDictionary) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		
		dictionary.setValue(self.currently?.dictionaryRepresentation(), forKey: "currently")
		dictionary.setValue(self.hourly?.dictionaryRepresentation(), forKey: "hourly")
		dictionary.setValue(self.daily?.dictionaryRepresentation(), forKey: "daily")

		return dictionary
	}

}
