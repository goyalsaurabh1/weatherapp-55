//
//  Currently.swift
//  WeatherAppFiftyFive
//
//  Created by SaurabhGoyal on 23/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//

import Foundation

public class Currently {
	public var time : Double?
	public var summary : String?
	public var icon : String?
	public var nearestStormDistance : Int?
	public var nearestStormBearing : Int?
	public var precipIntensity : Int?
	public var precipProbability : Double?
	public var temperature : Double?
	public var apparentTemperature : Double?
	public var dewPoint : Double?
	public var humidity : Double?
	public var pressure : Double?
	public var windSpeed : Double?
	public var windGust : Double?
	public var windBearing : Int?
	public var cloudCover : Double?
	public var uvIndex : Int?
	public var visibility : Double?
	public var ozone : Double?

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let currently = Currently(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Currently Instance.
*/
	required public init?(dictionary: NSDictionary) {

		time = dictionary["time"] as? Double
		summary = dictionary["summary"] as? String
		icon = dictionary["icon"] as? String
		nearestStormDistance = dictionary["nearestStormDistance"] as? Int
		nearestStormBearing = dictionary["nearestStormBearing"] as? Int
		precipIntensity = dictionary["precipIntensity"] as? Int
		precipProbability = dictionary["precipProbability"] as? Double
		temperature = dictionary["temperature"] as? Double
		apparentTemperature = dictionary["apparentTemperature"] as? Double
		dewPoint = dictionary["dewPoint"] as? Double
		humidity = dictionary["humidity"] as? Double
		pressure = dictionary["pressure"] as? Double
		windSpeed = dictionary["windSpeed"] as? Double
		windGust = dictionary["windGust"] as? Double
		windBearing = dictionary["windBearing"] as? Int
		cloudCover = dictionary["cloudCover"] as? Double
		uvIndex = dictionary["uvIndex"] as? Int
		visibility = dictionary["visibility"] as? Double
		ozone = dictionary["ozone"] as? Double
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.time, forKey: "time")
		dictionary.setValue(self.summary, forKey: "summary")
		dictionary.setValue(self.icon, forKey: "icon")
		dictionary.setValue(self.nearestStormDistance, forKey: "nearestStormDistance")
		dictionary.setValue(self.nearestStormBearing, forKey: "nearestStormBearing")
		dictionary.setValue(self.precipIntensity, forKey: "precipIntensity")
		dictionary.setValue(self.precipProbability, forKey: "precipProbability")
		dictionary.setValue(self.temperature, forKey: "temperature")
		dictionary.setValue(self.apparentTemperature, forKey: "apparentTemperature")
		dictionary.setValue(self.dewPoint, forKey: "dewPoint")
		dictionary.setValue(self.humidity, forKey: "humidity")
		dictionary.setValue(self.pressure, forKey: "pressure")
		dictionary.setValue(self.windSpeed, forKey: "windSpeed")
		dictionary.setValue(self.windGust, forKey: "windGust")
		dictionary.setValue(self.windBearing, forKey: "windBearing")
		dictionary.setValue(self.cloudCover, forKey: "cloudCover")
		dictionary.setValue(self.uvIndex, forKey: "uvIndex")
		dictionary.setValue(self.visibility, forKey: "visibility")
		dictionary.setValue(self.ozone, forKey: "ozone")

		return dictionary
	}

}
