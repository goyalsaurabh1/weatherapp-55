//
//  WeatherData.swift
//  WeatherAppFiftyFive
//
//  Created by SaurabhGoyal on 23/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//

import Foundation

public class WeatherData {
	public var time : Double?
	public var summary : String?
	public var icon : String?
	public var precipProbability : Int?
	
	public var humidity : Double?
	public var pressure : Double?
	public var windSpeed : Double?
	public var uvIndex : Int?
	public var uvIndexTime : Int?
	public var visibility : Double?
    public var temperature : Double?
	public var temperatureMin : Double?
	public var temperatureMax : Double?
   

/**
    Returns an array of models based on given dictionary.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [WeatherData]
    {
        var models:[WeatherData] = []
        for item in array
        {
            models.append(WeatherData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
*/
	required public init?(dictionary: NSDictionary) {

		time = dictionary["time"] as? Double
		summary = dictionary["summary"] as? String
		icon = dictionary["icon"] as? String
		precipProbability = dictionary["precipProbability"] as? Int
        temperature = dictionary["temperature"] as? Double
        humidity = dictionary["humidity"] as? Double
		pressure = dictionary["pressure"] as? Double
		windSpeed = dictionary["windSpeed"] as? Double
		uvIndex = dictionary["uvIndex"] as? Int
		uvIndexTime = dictionary["uvIndexTime"] as? Int
		visibility = dictionary["visibility"] as? Double
		temperatureMin = dictionary["temperatureMin"] as? Double
		temperatureMax = dictionary["temperatureMax"] as? Double
		
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.time, forKey: "time")
		dictionary.setValue(self.summary, forKey: "summary")
		dictionary.setValue(self.icon, forKey: "icon")
		dictionary.setValue(self.precipProbability, forKey: "precipProbability")
        dictionary.setValue(self.temperature, forKey: "temperature")
		dictionary.setValue(self.humidity, forKey: "humidity")
		dictionary.setValue(self.pressure, forKey: "pressure")
		dictionary.setValue(self.windSpeed, forKey: "windSpeed")
		dictionary.setValue(self.uvIndex, forKey: "uvIndex")
		dictionary.setValue(self.uvIndexTime, forKey: "uvIndexTime")
		dictionary.setValue(self.visibility, forKey: "visibility")
		dictionary.setValue(self.temperatureMin, forKey: "temperatureMin")
		dictionary.setValue(self.temperatureMax, forKey: "temperatureMax")

		return dictionary
	}

}
