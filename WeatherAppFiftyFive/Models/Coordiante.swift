//
//  Coordiante.swift
//  WeatherAppFiftyFive
//
//  Created by SaurabhGoyal on 22/04/19.
//  Copyright © 2019 SaurabhGoyal. All rights reserved.
//

struct Coordinate: Codable {
    let latitude: Double
    let longitude: Double
}

extension Coordinate: CustomStringConvertible {
    var description: String {
        return "\(latitude),\(longitude)"
    }
}
